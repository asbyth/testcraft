#include "utils/macros.h"
#include "utils/crash.h"
#include "TestCraft.h"
#include "utils/ReliableException.h"
#include <stdexcept>

#ifdef WIN32

const char *describeException(DWORD code) {
    switch (code) {
        case EXCEPTION_ACCESS_VIOLATION:
            return "EXCEPTION_ACCESS_VIOLATION";
        case EXCEPTION_DATATYPE_MISALIGNMENT:
            return "EXCEPTION_DATATYPE_MISALIGNMENT";
        case EXCEPTION_BREAKPOINT:
            return "EXCEPTION_BREAKPOINT";
        case EXCEPTION_SINGLE_STEP:
            return "EXCEPTION_SINGLE_STEP";
        case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:
            return "EXCEPTION_ARRAY_BOUNDS_EXCEEDED";
        case EXCEPTION_FLT_DENORMAL_OPERAND:
            return "EXCEPTION_FLT_DENORMAL_OPERAND";
        case EXCEPTION_FLT_DIVIDE_BY_ZERO:
            return "EXCEPTION_FLT_DIVIDE_BY_ZERO";
        case EXCEPTION_FLT_INEXACT_RESULT:
            return "EXCEPTION_FLT_INEXACT_RESULT";
        case EXCEPTION_FLT_INVALID_OPERATION:
            return "EXCEPTION_FLT_INVALID_OPERATION";
        case EXCEPTION_FLT_OVERFLOW:
            return "EXCEPTION_FLT_OVERFLOW";
        case EXCEPTION_FLT_STACK_CHECK:
            return "EXCEPTION_FLT_STACK_CHECK";
        case EXCEPTION_FLT_UNDERFLOW:
            return "EXCEPTION_FLT_UNDERFLOW";
        case EXCEPTION_INT_DIVIDE_BY_ZERO:
            return "EXCEPTION_INT_DIVIDE_BY_ZERO";
        case EXCEPTION_INT_OVERFLOW:
            return "EXCEPTION_INT_OVERFLOW";
        case EXCEPTION_PRIV_INSTRUCTION:
            return "EXCEPTION_PRIV_INSTRUCTION";
        case EXCEPTION_IN_PAGE_ERROR:
            return "EXCEPTION_IN_PAGE_ERROR";
        case EXCEPTION_ILLEGAL_INSTRUCTION:
            return "EXCEPTION_ILLEGAL_INSTRUCTION";
        case EXCEPTION_NONCONTINUABLE_EXCEPTION:
            return "EXCEPTION_NONCONTINUABLE_EXCEPTION";
        case EXCEPTION_STACK_OVERFLOW:
            return "EXCEPTION_STACK_OVERFLOW";
        case EXCEPTION_INVALID_DISPOSITION:
            return "EXCEPTION_INVALID_DISPOSITION";
        case EXCEPTION_GUARD_PAGE:
            return "EXCEPTION_GUARD_PAGE";
        case EXCEPTION_INVALID_HANDLE:
            return "EXCEPTION_INVALID_HANDLE";
        default: {
            char *buffer = (char *) malloc(50);
            sprintf(buffer, "[Unknown Win32 exception 0x%lx]", code);
            return buffer;
        }
    }
}

long WINAPI exceptionHandler(PEXCEPTION_POINTERS exceptionInfo) {
    CrashHandler::crash(describeException(exceptionInfo->ExceptionRecord->ExceptionCode), exceptionInfo->ContextRecord);
    return EXCEPTION_CONTINUE_SEARCH;
}

void setExceptionHandlers() {
    SetUnhandledExceptionFilter(exceptionHandler);
}

#elif __APPLE__

void setExceptionHandlers() {
    warn("macOS only has noop functions!")
}

#else
#error Platform not supported. To contributors: Please write some exception handler code!
#endif

int main(int argc, char *argv[]) {
    try {
        setExceptionHandlers();
        TestCraft(argc, argv).run();
        /*info("Initializing TestCraft %s", VersionHandler::GetVersion().c_str())
        Window window;
        window.Create();

        bgfx::setDebug(BGFX_DEBUG_TEXT);

        window.SetVsyncEnabled(false);
        window.SetResizeHandler([&] (int width, int height) {
            info("Resized to %d, %d", width, height)
        });
        glfwSetTime(0);
        double renderTime = 0.0;
        double lastDisplayUpdate = -2000.0;
        double frameTime = 0.0;
        while (!window.ShouldClose()) {
            auto start = glfwGetTime();
            auto size = window.GetSize();
            bgfx::setViewRect(0, 0, 0, size.x, size.y);
            bgfx::touch(0);
            bgfx::dbgTextClear();
            auto current = glfwGetTime();
            if (current - lastDisplayUpdate > 1.0) {
                renderTime = current - start;
            }
            bgfx::dbgTextPrintf(0, 0, 0x01, "Render time: %.2f ms (%.2f FPS)", renderTime * 1000.0, 1.0 / renderTime);
            bgfx::dbgTextPrintf(0, 1, 0x01, "Frame time:  %.2f ms (%.2f FPS)", frameTime * 1000.0, 1.0 / frameTime);
            window.Update();
            if (current - lastDisplayUpdate > 1.0) {
                frameTime = glfwGetTime() - start;
                lastDisplayUpdate = current;
            }
        }
        window.Destroy();*/

    } catch (const ReliableException& e) {
        e.crash();
    } catch (const std::exception &e) {
        CrashHandler::crash((e.what() + std::string(" (note: stack trace not reliable)")).c_str());
    }

    return 0;
}

#ifdef WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow) {
    main(__argc, __argv);
}
#endif