#ifndef TESTCRAFT_TESTCRAFT_H
#define TESTCRAFT_TESTCRAFT_H

#include <vector>
#include "rendering/Window.h"

class TestCraft {
public:
    TestCraft(int argc, char* argv[]);

    void run();

    Window& getWindow();

private:
    void init();

    Window window;
    std::vector<char*> args;
};


#endif //TESTCRAFT_TESTCRAFT_H
