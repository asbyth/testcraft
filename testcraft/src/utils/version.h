#ifndef TESTCRAFT_VERSION_H
#define TESTCRAFT_VERSION_H

#include <string>

class VersionHandler {
public:
    static std::string GetVersion();
};

#endif //TESTCRAFT_VERSION_H
