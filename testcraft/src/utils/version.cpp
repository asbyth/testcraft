#include <git.h>
#include "version.h"

std::string VersionHandler::GetVersion() {
    static std::string version;
    if (version.empty()) {
        version = GitMetadata::CommitShortSHA1();
        if (!GitMetadata::GetLastTag().empty()) {
            if (GitMetadata::GetLastTagCommit() == GitMetadata::CommitSHA1()) {
                version = GitMetadata::GetLastTag();
                if (GitMetadata::AnyUncommittedChanges()) {
                    version += "-SNAPSHOT";
                }
            } else {
                version = GitMetadata::GetLastTag() + "-" + version + "-SNAPSHOT";
            }
        } else {
            version += "-SNAPSHOT";
        }
    }
    return version;
}
