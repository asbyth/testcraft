#ifndef TESTCRAFT_RELIABLEEXCEPTION_H
#define TESTCRAFT_RELIABLEEXCEPTION_H

#include <stdexcept>

class ReliableException : public std::runtime_error {
public:
    explicit ReliableException(const char* message);

    void crash() const;

private:
    void* platformObj = nullptr;
};


#endif //TESTCRAFT_RELIABLEEXCEPTION_H
