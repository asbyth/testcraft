#ifndef TESTCRAFT_CRASH_H
#define TESTCRAFT_CRASH_H

#ifdef WIN32
#include <windows.h>
#endif

class CrashHandler {
public:
    static void crash(const char* description);

#ifdef WIN32
    static void crash(const char* description, CONTEXT* ctx);
#endif
};

#endif //TESTCRAFT_CRASH_H
