#include "ReliableException.h"
#include "crash.h"

ReliableException::ReliableException(const char *message) : runtime_error(message) {
#ifdef WIN32
    CONTEXT context = {};
    context.ContextFlags = CONTEXT_FULL;
    RtlCaptureContext(&context);
    platformObj = &context;
#endif
}

void ReliableException::crash() const {
#ifdef WIN32
    CrashHandler::crash(what(), (CONTEXT*) platformObj);
#else
    CrashHandler::crash(what());
#endif
}
