#include "crash.h"
#include "version.h"
#include "../rendering/Window.h"
#include <cstdio>
#include <bgfx/bgfx.h>

#ifdef WIN32

#include <dbghelp.h>
#include <git.h>

const int MaxNameLen = 256;

void printStack(CONTEXT *ctx) {
    BOOL result;
    HANDLE process;
    HANDLE thread;
    HMODULE hModule;

    STACKFRAME64 stack;
    ULONG frame;
    DWORD64 displacement;

    DWORD disp;
    IMAGEHLP_LINE64 *line;

    char buffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(TCHAR)];
    char name[MaxNameLen];
    char module[MaxNameLen];
    PSYMBOL_INFO pSymbol = (PSYMBOL_INFO) buffer;

    memset(&stack, 0, sizeof(STACKFRAME64));

    process = GetCurrentProcess();
    thread = GetCurrentThread();
    displacement = 0;
#if !defined(_M_AMD64)
    stack.AddrPC.Offset = (*ctx).Eip;
    stack.AddrPC.Mode = AddrModeFlat;
    stack.AddrStack.Offset = (*ctx).Esp;
    stack.AddrStack.Mode = AddrModeFlat;
    stack.AddrFrame.Offset = (*ctx).Ebp;
    stack.AddrFrame.Mode = AddrModeFlat;
#endif

    SymInitialize(process, NULL, TRUE); //load symbols

    for (frame = 0;; frame++) {
        //get next call from stack
        result = StackWalk64
                (
#if defined(_M_AMD64)
                IMAGE_FILE_MACHINE_AMD64
#else
                IMAGE_FILE_MACHINE_I386
#endif
                ,
                process,
                thread,
                &stack,
                ctx,
                nullptr,
                SymFunctionTableAccess64,
                SymGetModuleBase64,
                nullptr
        );

        if (!result) {
            if (frame == 0) fprintf(stderr, " (couldn't get current stack trace)\n");
            break;
        }

        //get symbol name for address
        pSymbol->SizeOfStruct = sizeof(SYMBOL_INFO);
        pSymbol->MaxNameLen = MAX_SYM_NAME;
        SymFromAddr(process, (ULONG64) stack.AddrPC.Offset, &displacement, pSymbol);

        line = (IMAGEHLP_LINE64 *) malloc(sizeof(IMAGEHLP_LINE64));
        line->SizeOfStruct = sizeof(IMAGEHLP_LINE64);

        if (SymGetLineFromAddr64(process, stack.AddrPC.Offset, &disp, line)) {
            fprintf(stderr, " %s (%s:%lu, %llx)\n", pSymbol->Name, line->FileName, line->LineNumber, pSymbol->Address);
        } else {
            hModule = nullptr;
            lstrcpyA(module, "");
            GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
                              (LPCTSTR) (stack.AddrPC.Offset), &hModule);

            //at least print module name
            if (hModule != nullptr) {
                GetModuleFileNameA(hModule, module, MaxNameLen);
                fprintf(stderr, " %s (%s, %llu)\n", pSymbol->Name, module, pSymbol->Address);
            } else {
                fprintf(stderr, " %s (%llu)\n", pSymbol->Name, pSymbol->Address);
            }
        }

        free(line);
    }
}

#elif __APPLE__

#elif __linux__

#else
#error Platform not supported (yet). We probably do want someone to add it though!
#endif

void printCrashHeader(const char *desc) {
    fprintf(stderr, "---- TestCraft Crash Report ----\n\n");
    fprintf(stderr, "Description: %s\n\nStack trace:\n", desc);
}

#define STR(x) #x

void printCrashDetails() {
    fprintf(stderr, "Details:\n");
    fprintf(stderr, "  Version: %s\n", VersionHandler::GetVersion().c_str());
    fprintf(stderr, "  Dirty source tree: %s\n", GitMetadata::AnyUncommittedChanges() ? "Yes" : "No");
    fprintf(stderr, "  Last commit: %s by %s (%s)\n", GitMetadata::CommitSubject().c_str(),
            GitMetadata::AuthorName().c_str(), GitMetadata::CommitSHA1().c_str());
    fprintf(stderr, "  On tag: %s\n", GitMetadata::GetLastTagCommit() == GitMetadata::CommitSHA1() ? "Yes" : "No");
    fprintf(stderr, "  Last tag: %s (%s)\n", GitMetadata::GetLastTag().c_str(),
            GitMetadata::GetLastTagCommit().c_str());
    fprintf(stderr, "  Platform: %s\n",
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#ifdef _WIN64
            "Windows (64-bit)"
#else
            "Windows (32-bit)"
#endif
#elif __APPLE__
#include <TargetConditionals.h>
#if TARGET_IPHONE_SIMULATOR
    "iOS Simulator"
#elif TARGET_OS_IPHONE
    "iOS"
#elif TARGET_OS_MAC
   "macOS"
#else
#error "Unknown Apple OS"
#endif
#elif __linux__
"Linux"
#else
#error "Unsupported platform"
#endif
    );
    fprintf(stderr, "  Libraries: %s\n", TESTCRAFT_LINKED_LIBRARIES);
    fprintf(stderr, "  BGFX initialized: %s\n", Window::IsBgfxInitialized ? "Yes" : "No");
    if (Window::IsBgfxInitialized) {
        fprintf(stderr, "  BGFX renderer: %s\n", bgfx::getRendererName(bgfx::getRendererType()));
    }
}

void CrashHandler::crash(const char *description) {
#ifdef WIN32
    CONTEXT context = {};
    context.ContextFlags = CONTEXT_FULL;
    RtlCaptureContext(&context);
    crash(description, &context);
#else
    printCrashHeader(description);
    fprintf(stderr, "\n");
    printCrashDetails();
#endif
    exit(1);
}

#ifdef WIN32

void CrashHandler::crash(const char *description, CONTEXT *ctx) {
    printCrashHeader(description);
    printStack(ctx);
    fprintf(stderr, "\n");
    printCrashDetails();
    exit(1);
}

#endif
