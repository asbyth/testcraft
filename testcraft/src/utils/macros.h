#ifndef TESTCRAFT_MACROS_H
#define TESTCRAFT_MACROS_H

#include <cstring>
#include <cstdio>

#ifdef WIN32
#define PATH_SEPERATOR '\\'
#define SEPERATOR ';'
#else
#define PATH_SEPERATOR '/'
#define SEPERATOR ':'
#endif

#define __FILENAME__ (strrchr(__FILE__, PATH_SEPERATOR) ? strrchr(__FILE__, PATH_SEPERATOR) + 1 : __FILE__)

#ifndef LOG_DEBUG
#define LOG_DEBUG 0
#endif

#define tc_log(location, format, level, ...) fprintf(location, "[%s:%i (%s)] [%s] " format "\n", __FILENAME__, __LINE__, __FUNCTION__, level, ##__VA_ARGS__)

#if LOG_DEBUG
#define debug(format, ...) tc_log(stdout, format, "DEBUG", ##__VA_ARGS__ );
#else
#define debug(format, ...) ;
#endif

#define info(format, ...) tc_log(stdout, format, "INFO", ##__VA_ARGS__ );

#define warn(format, ...) tc_log(stdout, format, "WARN", ##__VA_ARGS__ );

#define error(format, ...) tc_log(stdout, format, "ERROR", ##__VA_ARGS__ );

#endif //TESTCRAFT_MACROS_H
