#include "TestCraft.h"

TestCraft::TestCraft(int argc, char **argv) {
    for (int i = 0; i < argc; i++) {
        args.push_back(argv[i]);
    }
}

void TestCraft::run() {
    init();
    window.SetVsyncEnabled(false);
    bgfx::setDebug(BGFX_DEBUG_TEXT);
    glfwSetTime(0);
    double renderTime = 0.0;
    double lastDisplayUpdate = -2000.0;
    double frameTime = 0.0;
    while (!window.ShouldClose()) {
        auto start = glfwGetTime();
        auto size = window.GetSize();
        bgfx::setViewRect(0, 0, 0, size.x, size.y);
        bgfx::touch(0);
        bgfx::dbgTextClear();
        auto current = glfwGetTime();
        if (current - lastDisplayUpdate > 1.0) {
            renderTime = current - start;
        }
        bgfx::dbgTextPrintf(0, 0, 0x01, "Render time:   %.2f ms (%.2f FPS)", renderTime * 1000.0, 1.0 / renderTime);
        bgfx::dbgTextPrintf(0, 1, 0x01, "Frame time:    %.2f ms (%.2f FPS)", frameTime * 1000.0, 1.0 / frameTime);
        bgfx::dbgTextPrintf(0, 2, 0x01, "Vsync enabled: %s", window.IsVsyncEnabled() ? "Yes" : "No");
        bgfx::dbgTextPrintf(0, 3, 0x01, "Renderer: %s", bgfx::getRendererName(bgfx::getRendererType()));
        Window::Update();
        if (current - lastDisplayUpdate > 1.0) {
            frameTime = glfwGetTime() - start;
            lastDisplayUpdate = current;
        }
    }
    window.Destroy();
}

Window& TestCraft::getWindow() {
    return window;
}

void TestCraft::init() {
    window.Create();
}
