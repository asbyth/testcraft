#ifndef TESTCRAFT_RESOURCEPACK_H
#define TESTCRAFT_RESOURCEPACK_H

#include <string>
#include <SFML/System/InputStream.hpp>

class ResourcePack {
public:
    virtual std::string getName() = 0;

    virtual sf::InputStream getInputStream(std::string resource) = 0;

    virtual bool resourceExists(std::string resource) = 0;
};

#endif //TESTCRAFT_RESOURCEPACK_H
