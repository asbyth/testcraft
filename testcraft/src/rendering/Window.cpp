#include "Window.h"
#include <GLFW/glfw3native.h>
#include <stdexcept>
#include <SFML/Graphics/Image.hpp>
#include <utility>
#include "../utils/version.h"
#include "../utils/macros.h"
#include "../utils/crash.h"

#define ENSURE_WINDOW_CREATED if (!Handle) throw std::runtime_error("Window isn't created yet");

bool Window::IsBgfxInitialized = false;

int RendererPriorities[] = {
        -1000, // Noop - 0
        3000, // Direct3D 9 - 1
        4000, // Direct3D 11 - 2
        5000, // Direct3D 12 - 3
        6000, // GNM - 4
        8000, // Metal - 5
        7000, // NVN - 6
        0, // OpenGL ES 2.0+ - 7
        1000, // OpenGL 2.1+ - 8
        2000, // Vulkan, - 9
        -999 // Count - 10
};

int ActiveWindows = 0;

GLFWwindow *Window::GetHandle() {
    return Handle;
}

Window::Window() {
    SetTitle("TestCraft " + VersionHandler::GetVersion());
}

Window::~Window() {
    if (Handle) {
        Destroy();
    }
}

void Window::SetResizeHandler(std::function<void(int, int)> handler) {
    ResizeHandler = std::move(handler);
}

void Window::SetTitle(const std::string& title) {
    if (Title == title) return;
    if (Handle) {
        glfwSetWindowTitle(Handle, title.c_str());
    }
    Title = title;
}

std::string Window::GetTitle() {
    return Title;
}

glm::ivec2 Window::GetSize() {
    if (Handle) {
        glm::ivec2 size = {};
        glfwGetWindowSize(Handle, &size.x, &size.y);
        return size;
    } else {
        return Size;
    }
}

void Window::SetSize(glm::ivec2 size) {
    if (Size == size) return;
    if (Handle) {
        glfwSetWindowSize(Handle, size.x, size.y);
    }
    Size = size;
}

bool Window::IsVisible() {
    return Visible;
}

void Window::SetVisible(bool visible) {
    if (Visible == visible) return;
    /*if (Handle) {
        if (visible) {
            glfwShowWindow(Handle);
        } else {
            glfwHideWindow(Handle);
        }
    }
    Visible = visible;*/
}

bool Window::IsResizable() {
    return Resizable;
}

void Window::SetResizable(bool resizable) {
    if (Resizable == resizable) return;
    if (Handle) {
        throw std::runtime_error("Can't edit resizability while opened");
    }
    Resizable = resizable;
}

bool Window::IsVsyncEnabled() {
    return Vsync;
}

void Window::SetVsyncEnabled(bool vsync) {
    if (Vsync == vsync) return;
    if (Handle) {
        auto flags = BGFX_RESET_NONE;
        if (vsync) {
            flags |= BGFX_RESET_VSYNC;
        }
        bgfx::reset(Size.x, Size.y, flags, textureFormat);
    }
    Vsync = vsync;
}

glm::ivec2 Window::GetPosition() {
    if (Handle) {
        glm::ivec2 position = {};
        glfwGetWindowPos(Handle, &position.x, &position.y);
        return position;
    } else {
        return Position;
    }
}

void Window::SetPosition(glm::ivec2 position) {
    if (Position == position) return;
    if (Handle) {
        glfwSetWindowPos(Handle, position.x, position.y);
    }
    Position = position;
}

bool Window::ShouldClose() {
    ENSURE_WINDOW_CREATED
    return glfwWindowShouldClose(Handle);
}

void Window::SetShouldClose(bool shouldClose) {
    ENSURE_WINDOW_CREATED
    glfwSetWindowShouldClose(Handle, shouldClose);
}

void Window::Update() {
    glfwPollEvents();
    bgfx::frame();
}

void OnResize(GLFWwindow* handle, int width, int height) {
    auto window = (Window*) glfwGetWindowUserPointer(handle);
    auto flags = BGFX_RESET_NONE;
    if (window->IsVsyncEnabled()) {
        flags |= BGFX_RESET_VSYNC;
    }
    bgfx::reset(width, height, flags, window->GetTextureFormat());
    auto handler = window->GetResizeHandler();
    if (handler) handler(width, height);
}

void Window::Create() {
    if (Handle) Destroy();
    if (ActiveWindows == 0) {
        debug("Initializing GLFW")
        if (!glfwInit()) {
            throw std::runtime_error("Couldn't initialize GLFW");
        }
    }
    glfwDefaultWindowHints();
    // glfwWindowHint(GLFW_VISIBLE, Visible);
    glfwWindowHint(GLFW_RESIZABLE, Resizable);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    debug("Window Hints: Visible = %d, Resizable = %d", Visible, Resizable)
#ifdef __MACOSX__
    glfwWindowHint(GLFW_COCOA_RETINA_FRAMEBUFFER, false);
    debug("Turned off retina framebuffer")
#endif
    debug("Attempting to load icon")

    // TODO: Actually load from file
    sf::Image img;
    GLFWimage image;
    if (img.loadFromFile("resources/logo.png")) {
        image.pixels = const_cast<unsigned char *>(img.getPixelsPtr());
        image.width = img.getSize().x;
        image.height = img.getSize().y;
    } else {
        warn("Failed to load resources/logo.png")
        unsigned int pixels[16 * 16];
        for (int y = 0; y < 8; y++) {
            for (int x = 0; x < 8; x++) {
                int index = y * 16 + x;
                pixels[index] = 0xFFDC00FF;
            }
        }
        for (int y = 8; y < 16; y++) {
            for (int x = 8; x < 16; x++) {
                int index = y * 16 + x;
                pixels[index] = 0xFFDC00FF;
            }
        }
        for (int y = 0; y < 8; y++) {
            for (int x = 8; x < 16; x++) {
                int index = y * 16 + x;
                pixels[index] = 0xFF000000;
            }
        }
        for (int y = 8; y < 16; y++) {
            for (int x = 0; x < 8; x++) {
                int index = y * 16 + x;
                pixels[index] = 0xFF000000;
            }
        }
        image.width = 16;
        image.height = 16;
        image.pixels = (unsigned char*) pixels;
    }

    Handle = glfwCreateWindow(Size.x, Size.y, Title.c_str(), nullptr, nullptr);
    if (!Handle) throw std::runtime_error("Failed to create GLFW window");
    debug("Successfully created handle for this (%p) at %p", this, Handle)

    debug("Setting icon")
    glfwSetWindowIcon(Handle, 1, &image);

    // TODO: Select renderer

    auto videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    auto size = GetSize();
    glfwSetWindowPos(Handle, (videoMode->width - size.x) / 2, (videoMode->height - size.y) / 2);

    debug("Initializing BGFX")

    bgfx::Init init;
    init.type = bgfx::RendererType::OpenGL; // TODO: Support other graphics libs - tbh i'm just lazy and don't want to recompile my shaders for every platform
    init.resolution.width = size.x;
    init.resolution.height = size.y;
    init.resolution.reset = BGFX_RESET_NONE;
    if (Vsync) {
        init.resolution.reset |= BGFX_RESET_VSYNC;
    }

#ifdef GLFW_EXPOSE_NATIVE_WIN32
    init.platformData.nwh = glfwGetWin32Window(Handle);
#elif defined(GLFW_EXPOSE_NATIVE_COCOA)
    init.platformData.nwh = glfwGetCocoaWindow(Handle);
#elif defined(GLFW_EXPOSE_NATIVE_X11)
    init.platformData.ndt = glfwGetX11Display();
    init.platformData.nwh = glfwGetX11Window(Handle);
#endif

    bgfx::init(init);
    textureFormat = init.resolution.format;

    glfwSetWindowUserPointer(Handle, this);

    glfwSetWindowSizeCallback(Handle, OnResize);

    bgfx::setViewClear(0, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x303030ff, 1.f, 0); // NOLINT(hicpp-signed-bitwise)

    debug("Window initialized!")
    ++ActiveWindows;
    IsBgfxInitialized = true;
}

std::function<void(int, int)> Window::GetResizeHandler() {
    return ResizeHandler;
}

bgfx::TextureFormat::Enum Window::GetTextureFormat() {
    ENSURE_WINDOW_CREATED
    return textureFormat;
}

void Window::Destroy() {
    ENSURE_WINDOW_CREATED
    bgfx::shutdown();
    IsBgfxInitialized = false;
    glfwDestroyWindow(Handle);
    Handle = nullptr;
    if (--ActiveWindows == 0) {
        glfwTerminate();
    }
}