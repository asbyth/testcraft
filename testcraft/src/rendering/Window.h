#ifndef TESTCRAFT_WINDOW_H
#define TESTCRAFT_WINDOW_H

#include <GLFW/glfw3.h>
#include <string>
#include <glm/glm.hpp>
#include <functional>
#include <bgfx/bgfx.h>

class Window {
public:
    static bool IsBgfxInitialized;

    Window();

    ~Window();

    GLFWwindow* GetHandle();

    void SetResizeHandler(std::function<void(int, int)> handler);

    std::function<void(int, int)> GetResizeHandler();

    void SetTitle(const std::string& title);

    std::string GetTitle();

    glm::ivec2 GetSize();

    void SetSize(glm::ivec2 size);

    bool IsVisible();

    void SetVisible(bool visible);

    bool IsResizable();

    void SetResizable(bool resizable);

    bool IsVsyncEnabled();

    void SetVsyncEnabled(bool vsync);

    glm::ivec2 GetPosition();

    void SetPosition(glm::ivec2 position);

    bool ShouldClose();

    void SetShouldClose(bool shouldClose);

    bgfx::TextureFormat::Enum GetTextureFormat();

    static void Update();

    void Create();

    void Destroy();

private:
    std::function<void(int, int)> ResizeHandler = nullptr;
    GLFWwindow* Handle = nullptr;
    std::string Title = "TestCraft";
    glm::ivec2 Size = {1280, 720};
    bool Visible = false;
    bool Resizable = true;
    bool Vsync = true;
    glm::ivec2 Position = {0, 0};
    bgfx::TextureFormat::Enum textureFormat = bgfx::TextureFormat::Count;
};


#endif //TESTCRAFT_WINDOW_H
