//
// Created by gerrit on 22-1-2020.
//

#ifndef TESTCRAFT_GIT_H
#define TESTCRAFT_GIT_H

#include <string>

class GitMetadata {
public:
    static bool Populated();

    static bool AnyUncommittedChanges();

    static std::string AuthorName();

    static std::string AuthorEmail();

    static std::string CommitSHA1();

    static std::string CommitDate();

    static std::string CommitSubject();

    static std::string CommitBody();

    static std::string GetLastTag();

    static std::string GetLastTagCommit();

    static std::string GetLastTagCommitShort();

    static std::string CommitShortSHA1();
};

#endif //TESTCRAFT_GIT_H
